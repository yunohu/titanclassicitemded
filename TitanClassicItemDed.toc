## Interface: 11305
## Title: Titan Panel Classic [|cffeda55fItemized Deductions|r] |cff00aa001.0.0.4|r
## Notes: Drop the cheapest item from your bags and other bag space freeing functions. Never have a full bag error again.
## Version: 1.0.0.4
## X-Date: 2020-10-30
## Author: Kernighan
## X-Email:
## X-Website:
## X-Category:
## SavedVariables: TitanItemDedSettings, TPIDCache
## OptionalDeps: Auctioneer, Informant
## Dependencies: TitanClassic
TitanClassicItemDed.xml
